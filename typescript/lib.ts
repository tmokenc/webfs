import { isDir, fileName, filePath, File, Directory, PartitionsResponse, fetchList, fetchPartitions } from "./api.js";
import { DEFAULT_PATH, FILE_EXT } from "./constants.js";
import * as utils from "./utils.js";

export let currentPath: string;

export class WebFs {
    mountPoint: Element;
    pathShow: HTMLUListElement;
    pathInput: HTMLInputElement;
    fileList: HTMLDivElement;
    statusBar: HTMLDivElement;
    lastSelected?: HTMLDivElement;
    partitions: PartitionsResponse;

    constructor(mountPoint: string) {
        let m = document.querySelector(mountPoint);
        
        if (!m) {
            throw new Error("The Object doesn't exist");
        }

        this.mountPoint = m;

        this.pathShow = document.createElement("ul");
        this.pathInput = document.createElement("input");
        this.fileList = document.createElement("div");
        this.statusBar = document.createElement("div");

        this.pathShow.classList.add("path-display");
        this.fileList.classList.add("file-list", "flex", "flex-wrap", "scroll-overflow");

        this.mountPoint.appendChild(this.pathShow);
        this.mountPoint.appendChild(this.pathInput);
        this.mountPoint.appendChild(this.fileList);
        this.mountPoint.appendChild(this.statusBar);

        this.pathInput.addEventListener("keyup", (e: KeyboardEvent) => {
            if (e.keyCode === 13) {
                this.updateList()
            }
        });

        this.fileList.addEventListener("click", e => this.clickEntry(e));

        this.pathShow.addEventListener("click", (e: MouseEvent) => {
            const element = Array.from(document.elementsFromPoint(e.x, e.y))
                .find(e => e.classList.contains("path-part"));

            if (element) {
                const path = element.getAttribute("path");
                path && this.updateList(path);
            }
        });

        (async () => {
            this.partitions = await fetchPartitions();
            const initPath = localStorage.getItem("lastPath") || this.partitions.home ||DEFAULT_PATH;
            this.updateList(initPath);
        })();
    }

    clickEntry(e: MouseEvent) {
        const element = Array.from(document.elementsFromPoint(e.x, e.y))
            .find(e => e.classList.contains("dir-entry"));

        if (!element) {
            if (this.removeSelectedEntries() !== 0) {
                this.updateStatus();
            }
            return
        }

        const name = element.getAttribute("name");
        const isDir = element.getAttribute("isdir");

        if (element.getAttribute("clicked")) {
            let path = currentPath + "/" + name;
            let ext = element.getAttribute("ext");

            if (ext) {
                path += "." + ext;
            }

            if (isDir) {
                this.updateList(path);
            } else {
                openFile(path);
            }
        } else {
            element.setAttribute("clicked", "true");
            setTimeout(() => element.removeAttribute("clicked"), 500);
        }

        if (!e.ctrlKey) {
            this.removeSelectedEntries();
        }

        // if (e.shiftKey && this.lastSelected) {
        //     const entries = Array.from(this.fileList.getElementsByClassName("dir-entry"));
        //     const lastName = this.lastSelected.getAttribute("name");
        //     const lastIsDir = this.lastSelected.getAttribute("isdir");

        //     let selecting = false;

        //     for (const entry of entries) {
        //         if (selecting) {
        //             entry.classList.add("selected");
        //         }

        //         if (entry.getAttribute("name") !== lastName) {
        //             continue
        //         }

        //         if (entry.getAttribute("isdir") !== lastIsDir) {
        //             continue
        //         }
        //     }
        // }

        if (element.classList.contains("selected")) {
            if (Array.from(this.fileList.querySelectorAll(".dir-entry.selected")).length > 1) {
                element.classList.remove("selected");

            }
        } else {
            element.classList.add("selected");
            this.lastSelected = element as HTMLDivElement;
        }

        this.updateStatus();
    }

    removeSelectedEntries(): number {
        this.lastSelected = undefined;

        return Array.from(this.fileList.getElementsByClassName("selected"))
            .reduce((x, e) => {
                e.classList.remove("selected");
                return x + 1;
            }, 0)
    }

    showPath() {
        const paths = currentPath.split("/");

        paths.shift();

        this.pathShow.innerHTML = "";

        // create root path
        const element = document.createElement("li");
        element.classList.add("path-part", "point");
        element.textContent = "/";
        element.setAttribute("path", "/");

        this.pathShow.appendChild(element);
        // end

        let current = "";
        for (const path of paths) {
            current += "/" + path;

            const element = document.createElement("li");
            element.classList.add("path-part", "point");
            element.textContent = path;
            element.setAttribute("path", current);

            this.pathShow.appendChild(element);

        }
    }

    async updateList(p?: string) {
        const path = utils.pathResolve(p || this.pathInput.value);

        let list;
        try {
            list = await fetchList(path);
        } catch(err) {
            console.error(err);
            throw err;
        }

        currentPath = path;
        this.pathInput.value = path;
        this.showPath();
        localStorage.setItem("lastPath", path);
        console.log(list);
            
        this.fileList.innerHTML = "";
        this.fileList.setAttribute("free", list.free.toString());

        list.dirs.sort((a, b) => a.name.localeCompare(b.name));
        for (const dir of list.dirs) {
            const element = dirEntryToElement(dir);
            this.fileList.appendChild(element)
        }

        list.files.sort((a, b) => a.name.localeCompare(b.name));
        for (const file of list.files) {
            const element = dirEntryToElement(file);
            this.fileList.appendChild(element)
        }

        this.updateStatus();
    }

    updateStatus() {
        const [dirs, files] = Array.from(this.fileList.querySelectorAll(".dir-entry.selected"))
            .reduce(([dirs, files], e) => {
                if (e.getAttribute("isdir")) {
                    dirs += 1;
                } else {
                    files += 1;
                }

                return [dirs, files]
            }, [0, 0]);

        let status = "";

        if (dirs) {
            status += `${dirs} folders selected, `;
        }

        if (files) {
            status += `${files} items selected. `;
        }

        let freeSpace = Number(this.fileList.getAttribute("free")) || 0;
        let [calBytes, unit] = utils.calculateBytes(freeSpace);

        status += `Freespace ${calBytes} ${unit} (${freeSpace} B)`;
        this.statusBar.innerHTML = status;
    }
}

function dirEntryToElement(entry: Directory | File): Element {
    const element = document.createElement("div");
    element.classList.add("dir-entry", "point");

    element.setAttribute("name", entry.name);

    const coverElement = document.createElement("div");
    coverElement.classList.add("cover");

    const nameElement = document.createElement("p");
    nameElement.innerHTML += fileName(entry);

    if (isDir(entry)) {
        const directory = entry as Directory;
        element.setAttribute("isdir", "true");

        if (directory.cover) {
            const src = `/file?preview=true&p=${filePath(directory)}/${directory.cover}`;
            const element = `<img 
                src="${src}" 
                onerror="this.style.display = 'none'"
                class="center fit" 
                loading="lazy"
            >`;
            
            coverElement.innerHTML += element;
        }
    } else {
        const file = entry as File;

        if (file.ext) {
            element.setAttribute("ext", file.ext);
            let type = fileType(file.ext);

            if (type === FileType.Image || type === FileType.Video) {
                const src = "/file?preview=true&p=" + filePath(file);
                const element = `<img 
                    src="${src}" 
                    onerror="this.style.display = 'none'"
                    class="center fit" 
                    loading="lazy"
                >`;
                
                coverElement.innerHTML += element;

            }
        }
    }

    element.appendChild(coverElement);
    element.appendChild(nameElement);

    return element
}

const enum FileType {
    Image,
    Video,
    Music,
    Text,
    Other,
}

function fileType(e: string): FileType {
    let ext = e.split(".").pop();

    if (ext) {
        ext = ext.toLowerCase();
    } else {
        return FileType.Other;
    }

    if (FILE_EXT.TEXT.some(v => v === ext)) {
        return FileType.Text
    }

    if (FILE_EXT.IMAGE.some(v => v === ext)) {
        return FileType.Image
    }

    if (FILE_EXT.VIDEO.some(v => v === ext)) {
        return FileType.Video
    }

    if (FILE_EXT.MUSIC.some(v => v === ext)) {
        return FileType.Music
    }

    return FileType.Other
}

async function openFile(p: string) {
    const type = fileType(p);
    const path = "/file?p=" + p;

    let fileElement: Element;

    switch (type) {
        case FileType.Image:
            fileElement = document.createElement("img");
            (fileElement as HTMLImageElement).src = path;
            break;

        case FileType.Video:
            fileElement = document.createElement("video");
            (fileElement as HTMLVideoElement).controls = true;
            (fileElement as HTMLVideoElement).poster = path + "&preview=true"

            const video: HTMLSourceElement = document.createElement("source");
            video.src = path;

            fileElement.appendChild(video);
            break

        case FileType.Text:
            fileElement = document.createElement("pre");
            const code = document.createElement("code");

            try {
                const data = await fetch(path);
                code.innerText = await data.text();
            } catch(err) {
                console.error(err);
                return
            }
 
            utils.highlightElement(code);

            (code as HTMLPreElement).style.backgroundColor = "#FFFFFF";
            (code as HTMLPreElement).style.padding = "10px";

            fileElement.appendChild(code);
            break;

        default:
            return
    }

    fileElement.classList.add("fit", "scroll-overflow");

    const fileContainer = document.createElement("div");
    fileContainer.classList.add("center", "overlay-item");
    fileContainer.appendChild(fileElement); 
    const overlay = document.createElement("div");
    overlay.classList.add("overlay-tmp", "backlight");
    overlay.appendChild(fileContainer);

    overlay.addEventListener("click", function(e: MouseEvent) {
        const outside_check = Array.from(document.elementsFromPoint(e.x, e.y))
            .every(e => !e.classList.contains("overlay-item"));

        if (outside_check) {
            this.remove();
        }
    });

    document.body.appendChild(overlay);
}
