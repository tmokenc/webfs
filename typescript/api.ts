import { currentPath } from "./lib.js";
import { ENDPOINT } from "./constants.js";

interface ListResponse {
    total: number,
    free: number,
    path: string,
    dirs: [Directory],
    files: [File],
}

export interface DirEntry {
    name: string;
}

export interface Directory extends DirEntry {
    cover?: string;
}

export interface File extends DirEntry {
    ext?: string;
    size: number;
    readonly: boolean;
}

export function fileName(entry: Directory | File) {
    let name = entry.name;

    if ("ext" in entry) {
        name += "." + entry.ext;
    }

    return name
}

export function filePath(entry: Directory | File) {
    return currentPath + "/" + fileName(entry)
}

export function isDir(entry: Directory | File): entry is Directory {
    return (entry as File).size === undefined
}

export async function fetchList(path: string): Promise<ListResponse> {
    const body = JSON.stringify({
        p: path
    });

    const request = await fetch(ENDPOINT.LIST, {
        method: "POST",
        body,
        headers: {
            "Content-Type": "application/json"
        }
    });

    return request.json()
}

export interface PartitionsResponse {
    home?: string,
    list: [Partition]
}

export interface Partition {
    path: string,
    name: string,
    size: number,
    free: number,
}

export async function fetchPartitions(): Promise<PartitionsResponse> {
    const request = await fetch(ENDPOINT.PARTITIONS);
    return request.json();
}
