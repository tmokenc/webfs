export const DEFAULT_PATH = "/run/media/tmokenc/AC233";
export const BYTE_UNIT = ["B", "KB", "MB", "GB", "TB", "EB"];
export const FILE_EXT = {
    VIDEO: ["mp4"],
    MUSIC: ["mp3"],
    IMAGE: ["jpg", "png", "gif"],
    TEXT: ["txt", "rs", "ts", "js"],
};

export enum ENDPOINT {
    LIST = "/api/list",
    PARTITIONS = "/api/partitions"
}
