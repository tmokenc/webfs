import { BYTE_UNIT } from "./constants.js";

export function pathResolve(path: string) {
    let res = path.split("/").filter(e => e !== "").join("/");

    if (path.startsWith("/")) {
        res = "/" + res;
    }

    return res
}

export function calculateBytes(bytes: number): [number, string] {
    let calculated = bytes;
    let unit_type = "B";

    for (const unit of BYTE_UNIT) {
        unit_type = unit;

        if (calculated < 1024) {
            break;
        }

        calculated /= 1024;
    }

    return [Number(calculated.toFixed(2)), unit_type]
}

export const highlightElement = Function("el", "opt", `
    return hljs.highlightElement(el, opt);
`);
