use config::{Config, File};
use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use std::path::Path;

#[derive(Serialize, Deserialize)]
#[serde(default)]
pub struct WebConfig {
    ip: IpAddr,
    port: u16,
    log: String,
    preview_cache_size_limit: usize,
}

impl Default for WebConfig {
    fn default() -> Self {
        Self {
            ip: Ipv4Addr::LOCALHOST.into(),
            port: 8080,
            log: "info".to_string(),
            preview_cache_size_limit: 500 * 1024 * 1024,
        }
    }
}

impl WebConfig {
    pub fn new() -> Self {
        Config::default()
            .with_merged(File::from(Path::new("./config.toml")))
            .unwrap()
            .try_into()
            .unwrap()
    }

    pub fn address(&self) -> SocketAddr {
        SocketAddr::new(self.ip, self.port)
    }

    pub fn log(&self) -> &str {
        &self.log
    }
}
