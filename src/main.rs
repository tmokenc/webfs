#[macro_use]
extern crate serde;

use actix_files::Files;
use actix_web::{middleware, web, App, HttpServer};

mod config;
mod route;
mod utils;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let config = web::Data::new(config::WebConfig::new());

    std::env::set_var("RUST_LOG", config.log());
    pretty_env_logger::init();

    build_typescript()?;
    let db = web::Data::new(sled::open("./db")?);

    let address = config.address();

    if address.ip().is_unspecified() {
        log::info!("Current configurated IP is an unspecified IP address");
        if let Ok(ip) = utils::get_local_ip() {
            log::info!("Current local IP is: {}", ip);
        }
    }

    HttpServer::new(move || {
        App::new()
            .app_data(config.clone())
            .app_data(db.clone())
            .wrap(middleware::Logger::default())
            .service(Files::new("/static", "./static").prefer_utf8(true))
            .service(route::index)
            .service(route::file)
            .service(route::list_file)
            .service(route::partitions)
    })
    .bind(address)?
    .run()
    .await
}

fn build_typescript() -> std::io::Result<()> {
    log::info!("Building TypeScript files");
    let command = std::process::Command::new("tsc")
        .args(&["-p", "./tsconfig.json"])
        .output()?;

    if !command.stdout.is_empty() {
        let message = std::str::from_utf8(&command.stdout).unwrap();
        let error = std::io::Error::new(std::io::ErrorKind::Other, message);
        return Err(error);
    }

    if !command.stderr.is_empty() {
        let message = std::str::from_utf8(&command.stderr).unwrap();
        let error = std::io::Error::new(std::io::ErrorKind::Other, message);
        return Err(error);
    }

    log::info!("TypeScript build: Done!");
    Ok(())
}
