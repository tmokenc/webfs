use std::net::{IpAddr, UdpSocket};
use std::path::Path;
use std::time::Duration;
use std::time::SystemTime;

pub fn get_local_ip() -> std::io::Result<IpAddr> {
    let socket = UdpSocket::bind("0.0.0.0:12129")?;
    socket.connect("8.8.8.8:80")?;
    socket.local_addr().map(|v| v.ip())
}

pub fn last_modified(path: impl AsRef<Path>) -> std::io::Result<SystemTime> {
    let metadata = std::fs::metadata(path)?;

    metadata.modified().or_else(|_| metadata.created())
}

#[cfg(target_os = "android")]
pub async fn disk_usage(path: impl AsRef<Path>) -> (u64, u64) {
    let path = path.as_ref().to_owned();

    actix_web::web::block(move || {
        use sysinfo::{DiskExt, RefreshKind, System, SystemExt};
        let refesh = RefreshKind::new().with_disks();
        let mut system = System::new_with_specifics(refesh);
        let mut longest_path = 0;
        let mut current = (0, 0);

        system.refresh_disks_list();

        for disk in system.get_disks() {
            let disk_path = disk.get_mount_point();

            if path.starts_with(disk_path) {
                let len = disk_path.iter().count();

                if len > longest_path {
                    longest_path = len;
                    current = (disk.get_total_space(), disk.get_available_space());
                }
            }
        }

        std::io::Result::Ok(current)
    })
    .await
    .unwrap_or_default()
}

#[cfg(not(target_os = "android"))]
pub async fn disk_usage(path: impl AsRef<Path>) -> (u64, u64) {
    heim::disk::usage(&path)
        .await
        .map(|v| {
            (
                v.total().get::<heim::units::information::byte>(),
                v.free().get::<heim::units::information::byte>(),
            )
        })
        .unwrap_or_default()
}

pub fn get_video_duration(path: impl AsRef<Path>) -> Option<Duration> {
    let p = path.as_ref();
    let s = p.to_str()?;
    let command = std::process::Command::new("ffprobe")
        .args(&[
            "-v",
            "error",
            "-show_entries",
            "format=duration",
            "-of",
            "default=noprint_wrappers=1:nokey=1",
            &s,
        ])
        .output()
        .ok()?;

    if !command.stdout.is_empty() {
        let s = std::str::from_utf8(&command.stdout).ok()?;
        let num = s.split(".").next()?.parse::<u64>().ok()?;

        return Some(Duration::from_secs(num));
    }

    None
}

pub fn generate_video_preview(path: impl AsRef<Path>) -> Option<Vec<u8>> {
    let path = path.as_ref();
    let duration = get_video_duration(&path)?;
    let s = path.to_str()?;

    let preview_time = duration.as_secs() / 100 * 30;
    let start = preview_time.to_string();

    let command = std::process::Command::new("ffmpeg")
        .args(&[
            "-ss",
            start.as_str(),
            "-i",
            s,
            "-c:v",
            "mjpeg",
            "-frames:v",
            "1",
            "-q:v",
            "2",
            "-vf",
            "scale=300:300:force_original_aspect_ratio=decrease",
            "-loglevel",
            "quiet",
            "-f",
            "rawvideo",
            "pipe:1",
        ])
        .output()
        .ok()?;

    if !command.stdout.is_empty() {
        return Some(command.stdout);
    }

    None
}
