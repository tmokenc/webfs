use actix_files::NamedFile;
use actix_web::http::header;
use actix_web::{get, post, web, Error, HttpResponse, Responder};
use async_fs as fs;
use bytes::Bytes;
use futures::stream::TryStreamExt as _;
use std::path::{Path, PathBuf};
use std::sync::Arc;

type StdError = Box<dyn std::error::Error + Send + Sync>;

#[get("/")]
pub async fn index() -> std::io::Result<impl Responder> {
    NamedFile::open("./static/index.html")
}

#[derive(Deserialize)]
pub struct FileQuery {
    p: PathBuf,
    #[serde(default)]
    preview: bool,
    #[serde(default)]
    is_dir: bool,
}

fn update_preview_to_db(key: &[u8], value: &[u8], db: &sled::Tree) {
    const MAX_SIZE: u64 = 500 * 1024 * 1024; // 500MB
    let db_value_size = (key.len() + value.len()) as u64;

    if let Err(why) = db.insert(&key, value) {
        log::error!("Cannot insert preview thumbnail to db:\n{:#?}", why);
    } else {
        let size = db
            .fetch_and_update(b"/", move |v| {
                let res = match v {
                    None => Some(db_value_size),
                    Some(b) => bincode::deserialize::<u64>(b)
                        .map(|v| v + db_value_size)
                        .ok(),
                };

                res.and_then(|v| bincode::serialize(&v).ok())
            })
            .ok()
            .flatten()
            .and_then(|v| bincode::deserialize::<u64>(&*v).ok());

        if let Some(size) = size {
            if size + db_value_size > MAX_SIZE {
                db.pop_max().ok();
            }
        }
    }
}

fn generate_preview(path: impl AsRef<Path>, db: Arc<sled::Db>) -> Result<web::Bytes, StdError> {
    #[derive(Serialize, Deserialize)]
    struct PreviewValue {
        modified: Option<std::time::SystemTime>,
        bytes: Bytes,
    }

    let mut bytes = Vec::new();
    let path = path.as_ref();
    let db = db.open_tree("preview")?;

    let key = path.display().to_string().into_bytes();
    let last_modified = crate::utils::last_modified(&path).ok();

    if let Some(modified) = last_modified {
        let db_data = db
            .get(&key)
            .ok()
            .flatten()
            .and_then(|v| bincode::deserialize::<PreviewValue>(&*v).ok())
            .filter(|v| v.modified.filter(|&v| modified == v).is_some());

        if let Some(data) = db_data {
            return Ok(data.bytes);
        }
    }

    if path
        .extension()
        .and_then(|v| v.to_str())
        .filter(|&v| v == "mp4")
        .is_some()
    {
        let b =
            crate::utils::generate_video_preview(path).ok_or("Cannot generate video preview")?;
        bytes.extend_from_slice(&b)
    } else {
        // let image = image::open(path)?;
        //
        let image = image::io::Reader::open(path)?
            // .with_guessed_format()?
            .decode()?;

        image
            .thumbnail(300, 300)
            .write_to(&mut bytes, image::ImageFormat::Jpeg)?;
    }

    let value = PreviewValue {
        modified: last_modified,
        bytes: bytes.into(),
    };

    if let Ok(value) = bincode::serialize(&value) {
        update_preview_to_db(&key, &value, &db);
    }

    Ok(value.bytes)
}

#[get("/file")]
pub async fn file(
    req: web::HttpRequest,
    db: web::Data<sled::Db>,
    query: web::Query<FileQuery>,
) -> Result<HttpResponse, Error> {
    let query = query.into_inner();

    if query.preview {
        let path = query.p.to_owned();
        let db = db.into_inner();
        let preview = web::block(move || generate_preview(path, db)).await?;

        Ok(HttpResponse::Ok()
            .header(header::CONTENT_TYPE, "image/jpeg")
            .body(preview))
    } else {
        NamedFile::open(&query.p)?.into_response(&req)
    }
}

#[derive(Deserialize)]
pub struct RequestList {
    p: PathBuf,
}

#[derive(Serialize)]
struct List {
    total: u64,
    free: u64,
    path: PathBuf,
    dirs: Vec<DirectoryResponse>,
    files: Vec<FileResponse>,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
struct DirectoryResponse {
    name: String,
    cover: Option<String>,
}

#[derive(Serialize)]
struct FileResponse {
    name: String,
    size: u64,
    ext: Option<String>,
    readonly: bool,
}

#[post("/api/list")]
pub async fn list_file(json: web::Json<RequestList>) -> Result<HttpResponse, Error> {
    let request = json.into_inner();
    let path = request.p.canonicalize()?;
    let mut dir = fs::read_dir(&path).await?;
    let mut files = Vec::new();
    let mut dirs = Vec::new();

    let (total, free) = crate::utils::disk_usage(&path).await;

    while let Ok(Some(entry)) = dir.try_next().await {
        let metadata = match entry.metadata().await {
            Ok(res) => res,
            Err(_) => continue,
        };

        let path = entry.path();

        let name = path
            .file_stem()
            .and_then(|v| v.to_str())
            .map(|v| v.to_string());

        let ext = path
            .extension()
            .and_then(|v| v.to_str())
            .map(|v| v.to_string());

        let name = match name {
            Some(res) => res,
            None => continue,
        };

        if metadata.is_dir() {
            let cover = get_dir_cover(&path).await.ok().flatten();
            dirs.push(DirectoryResponse { name, cover });
        } else {
            files.push(FileResponse {
                name,
                ext,
                size: metadata.len(),
                readonly: metadata.permissions().readonly(),
            });
        }
    }

    let result = List {
        total,
        free,
        path,
        dirs,
        files,
    };

    Ok(HttpResponse::Ok().json(result))
}

async fn get_dir_cover(path: impl AsRef<Path>) -> std::io::Result<Option<String>> {
    let path = path.as_ref();
    let dir_name = match path.file_name().and_then(|v| v.to_str()) {
        Some(v) => v.to_string().to_lowercase(),
        None => return Ok(None),
    };

    let mut dir = fs::read_dir(path).await?;

    while let Some(entry) = dir.try_next().await? {
        let path = entry.path();
        let name = path
            .file_stem()
            .and_then(|v| v.to_str())
            .map(|v| v.to_string());

        let ext = path
            .extension()
            .and_then(|v| v.to_str())
            .map(|v| v.to_string());

        if let Some((name, ext)) = name.zip(ext) {
            let n = name.to_lowercase();
            if &ext != "jpg" && &ext != "png" {
                continue;
            }

            if n == dir_name || &n == "folder" || &n == "cover" {
                return Ok(Some(format!("{}.{}", name, ext)));
            }
        }
    }

    Ok(None)
}

#[derive(Serialize)]
pub struct PartitionsResponse {
    list: Vec<Partition>,
    home: Option<PathBuf>,
}

#[derive(Serialize)]
struct Partition {
    name: String,
    path: PathBuf,
    size: u64,
    free: u64,
}

#[get("/api/partitions")]
pub async fn partitions() -> Result<HttpResponse, Error> {
    let list = get_partitions().await;
    let home = dirs::home_dir();
    return Ok(HttpResponse::Ok().json(PartitionsResponse { home, list }));
}

#[cfg(not(target_os = "android"))]
async fn get_partitions() -> Vec<Partition> {
    let mut partitions = heim::disk::partitions_physical();
    let mut list = Vec::new();

    while let Ok(Some(partition)) = partitions.try_next().await {
        let path = partition.mount_point();
        let name = path.file_name().and_then(|v| v.to_str());

        if let Some(name) = name {
            if let Ok(usage) = heim::disk::usage(&path).await {
                list.push(Partition {
                    name: name.to_owned(),
                    path: path.to_owned(),
                    size: usage.total().get::<heim::units::information::byte>(),
                    free: usage.free().get::<heim::units::information::byte>(),
                });
            }
        }
    }

    list
}

#[cfg(target_os = "android")]
async fn get_partitions() -> Vec<Partition> {
    use sysinfo::{DiskExt, RefreshKind, System, SystemExt};

    web::block(|| {
        let refesh = RefreshKind::new().with_disks();
        let mut system = System::new_with_specifics(refesh);
        system.refresh_disks_list();

        let mut list = Vec::new();

        for disk in system.get_disks() {
            let path = disk.get_mount_point();
            let name = path.file_name().and_then(|v| v.to_str());

            if let Some(name) = name {
                list.push(Partition {
                    name: name.to_owned(),
                    path: path.to_owned(),
                    size: disk.get_total_space(),
                    free: disk.get_available_space(),
                });
            }
        }

        std::io::Result::Ok(list)
    })
    .await
    .unwrap_or_default()
}
